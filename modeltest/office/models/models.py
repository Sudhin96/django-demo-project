from django.db import models

# Create your models here.
class OfficeLocationTestNew(models.Model):
    class OfficeType(models.TextChoices):
        HEADQUARTERS = ('Headquarters')
        SATELLITE = ('Satellite')
        AREA = ('Area')
        REGIONAL = ('Regional')

    class AreasServedPrecision(models.TextChoices):
        NATIONWIDE = ('Nationwide')
        STATEWIDE = ('Statewide')
        COUNTYWIDE = ('Countywide')

    class NationwideChoices(models.TextChoices):
        ALL = ('All')
        CONTINENTALL = ('Continental')

    # organization = models.ForeignKey(Organization, related_name='office_locations', on_delete=models.CASCADE)

    # trade type(s) live here?

    # phone_number = PhoneNumberField(null=True, blank=True)
    phone_number = models.CharField(max_length=10)
    phone_number_extention = models.CharField(max_length=8, null=True, blank=True)
    address_1 = models.CharField(max_length=255)
    address_2 = models.CharField(max_length=255, null=True, blank=True)
    office_type = models.CharField(max_length=255, choices=OfficeType.choices, default=OfficeType.SATELLITE)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    zip_code = models.CharField(max_length=200, null=True)
    # location = PointField(null=True)
    location = models.CharField(max_length=10)
    # office_contacts = models.ManyToManyField('Employee', related_name="office_locations")
    office_contacts = models.CharField(max_length=200)
    # categories = models.ManyToManyField('TertiaryOrganizationCategory', related_name='office_locations', blank=True)
    categories = models.CharField(max_length=200)
    areas_served_precision = models.CharField(max_length=255, choices=AreasServedPrecision.choices, default=AreasServedPrecision.STATEWIDE)
    nationwide_selection = models.CharField(max_length=255, choices=NationwideChoices.choices, null=True, blank=True)
    areas_served = models.CharField(max_length=255, choices=NationwideChoices.choices, null=True, blank=True)
    # created_by = models.ForeignKey('organizations.Employee', null=True, blank=True, related_name="office_locations_created", on_delete=models.SET_NULL)
    created_by = models.CharField(max_length=200)
    # market_segments = models.ManyToManyField('MarketSegment', related_name='office_locations', blank=True)
    market_segments = models.CharField(max_length=200)

    @property
    def full_address(self):
        return f"{self.address_1},{self.address_2+',' if self.address_2 else ''} {self.city}, {self.state}"

    def __str__(self):
        return f"{'(HQ)' if self.office_type == self.OfficeType.HEADQUARTERS else ''} {self.city or ''} {self.state or ''} @ {self.location}"

    def save(self, *args, **kwargs):
        # self._clean()
        super().save(*args, **kwargs)

    # def _clean(self):
    #     filter_fields = {
    #         'city': self.city,
    #         'state': self.state,
    #         'address_1': self.address_1,
    #     }

    #     # locations = self.organization.office_locations.filter(Q(**filter_fields)).exclude(id=self.id if self.id else None)
    #     locations = self.city
    #     if (
    #         (locations and (
    #             not self.address_2 or
    #             (self.address_2 and locations.filter(Q(address_2__isnull=True) | Q(address_2='') | Q(address_2=self.address_2)).exists())
    #         ))
    #     ):
    #         if self.address_2:
    #             raise ValidationError({'address_2': ['Office already exists. Address 2 must be unique.']})
    #         raise ValidationError({'address_1': ['Office already exists. Specify a unique Address 2 on both offices to add.']})