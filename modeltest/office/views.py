from .models.models import OfficeLocationTestNew
from rest_framework.views import APIView 
from django.http import JsonResponse

from rest_framework.generics import CreateAPIView
 

# Create your views here.
class DemoAPIView(CreateAPIView):
    def get(self, *args, **kwargs):
        try:
            queryset_list = OfficeLocationTestNew.objects.all().values("id", "state", "address_1", "address_2", "city")
            if queryset_list:
                return JsonResponse({"status": "success", "result": list(queryset_list)})
            else:
                return JsonResponse({"status": "success", "result": "empty"})
        except Exception as ex:
            return JsonResponse({"status": "failure", "message": str(ex)})

class createLocation(CreateAPIView):
    def post(self, request):
        try:
            temp= OfficeLocationTestNew.objects.create(
                    phone_number=request.data.get('phone_number'),
                    address_1=request.data.get('address_1'),
                    address_2=request.data.get('address_2'),
                    city=request.data.get('city')
                )
            return JsonResponse({"status":'success'})
        except Exception as ex:
            return JsonResponse({"status": "failure", "message": str(ex)})
